<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');

Route::post('/courses/seed', "SeedCoursesController@store")->middleware("auth:api");
Route::post('/courses/register', 'RegisterCourseController@store');
Route::get("courses", 'CourseController@index')->middleware("auth:api");
Route::get('courses/export/', 'CourseController@export');

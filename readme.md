## SeamlessHR Interview Test

### Authentication

This project implements JWT for authentication using [this library](https://github.com/tymondesigns/jwt-auth). To configure this, the following environment variable in the `.env` file should be set:

-   **JWT_SECRET:** Could also be set using: `php artisan jwt:secret`

-   **JWT_PUBLIC_KEY:** This can be a path or resource
-   **JWT_PRIVATE_KEY:** This can be a path or resource
-   **JWT_PASSPHRASE:** The passphrase for your private key. Can be null if none set.

JWT token expires in 60 minutes and should be refreshed after that to stay authenticated. Refresh tokens expires in 2 weeks, after which a user would be required to login again to retrieve another JWT refresh token.

Endpoints for registration and authentication include:

`POST /api/register` Accepts username, email and password for registration
Request:

```bash
curl -X POST seamlesshr.test/api/register -H "Content-type: application/json" -H "Accept: application/json" -d '{"name": "Aleem isiaka", "email": "aleem@email.com", "password": "hgcghgbhviuviu"}'
```

Response:

```
{"access_token":"JWT_TOKEN","token_type":"bearer","expires_in":3600}
```

`POST /api/login` Accepts email and password to retrieve tokens.
Request:

```bash
curl -X POST seamlesshr.test/api/login -H "Content-type: application/json" -H "Accept: application/json" -d '{"email": "aleem@email.com", "password": "hgcghgbhviuviu"}'
```

Response:

```json
{
    "access_token": "JWT_TOKEN",
    "token_type": "bearer",
    "expires_in": 3600
}
```

`POST /api/logout` Would logout the auth token attached to the request.
Request:

```bash
curl -X POST seamlesshr.test/api/logout -H "Content-type: application/json" -H "Accept: application/json" -H "Authorization: Bearer JWT_TOKEN"
```

Response:

```json
{ "message": "User has been logged out successfully" }
```

---

## Additional columns to courses

3 more columns were added to the course table and they are:

-   **name**: A name for a course
-   **rating**: Total rating that a course has
-   **is_active**: Determines if a course is active or not

---

### Endpoint for seeding 50 courses

An endpoint is included for creating 50 courses at a time. To make this request, a `POST /api/courses/seed`. Each request to the endpoint generates 50 fake course records and stores them into the database.
Request:

```bash
curl -X POST seamlesshr.test/api/courses/seed -H "Content-type: application/json" -H "Accept: application/json" -H "Authorization: Bearer JWT_TOKEN"
```

Response:

```json
{ "message": "Queued 50 courses for seeding" }
```

---

### Register for course

#### Request:

```bash
curl -X POST seamlesshr.test/api/courses/register -H "Content-type: application/json" -H "Accept: application/json" -H "Authorization: Bearer JWT_TOKEN" -d '{"course_id": "1"}'
```

#### Response

```json
{
    "message": "Course registered",
    "data": {
        "course_id": "1",
        "user_id": 1,
        "updated_at": "2020-03-09 00:32:45",
        "created_at": "2020-03-09 00:32:45",
        "id": 3
    }
}
```

### List all available courses

To list all the courses available in the application, use the endpoint: `/api/courses`. It supports pagination.

```bash
curl -X GET seamlesshr.test/api/courses -H "Content-type: application/json" -H "Accept: application/json" -H "Authorization: Bearer JWT_TOKEN"
```

```json
{
    "data": [
        {
            "id": 1,
            "text": "Ea consequatur illo eius esse ex. Autem dolore sint qui ut rerum excepturi dolores. Repellat dolor qui illo hic perspiciatis asperiores. Consequuntur sed iusto asperiores.",
            "name": "Dulce West",
            "created_at": "2020-03-08 22:40:24",
            "updated_at": "2020-03-08 22:40:24",
            "user": null,
            "active": true,
            "rating": "2",
            "registered_at": "2020-03-09T00:32:45.000000Z"
        },

        {
            "id": 1,
            "text": "Ea in cupiditate atque blanditiis aspernatur enim iusto. Sunt facilis similique vel maxime nisi.",
            "name": "Dulce West",
            "created_at": "2020-03-08 22:40:24",
            "updated_at": "2020-03-08 22:40:24",
            "user": null,
            "active": true,
            "rating": "2",
            "registered_at": null
        }
    ],
    "links": {
        "first": "http://seamlesshr.test/api/courses?page=1",
        "last": "http://seamlesshr.test/api/courses?page=82",
        "prev": null,
        "next": "http://seamlesshr.test/api/courses?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 82,
        "path": "http://seamlesshr.test/api/courses",
        "per_page": 2,
        "to": 1,
        "total": 82
    }
}
```

---

### Download all courses to CSV

#### Request

```bash
curl -X GET seamlesshr.test/api/courses/export --output "./courses.csv"
```

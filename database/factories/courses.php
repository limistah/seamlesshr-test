<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name(),
        'text' => $faker->sentence(10),
        'active' => $faker->randomElement($array = array('true', 'false')),
        'rating' => $faker->randomElement($array = array('1', '2', '3', '4', '5'))
    ];
});

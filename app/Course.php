<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RegisteredCourses;

class Course extends Model
{
    protected $casts = [
        'active' => 'boolean',
    ];

    public function registered()
    {
        return $this->hasMany(RegisteredCourses::class);
    }
}

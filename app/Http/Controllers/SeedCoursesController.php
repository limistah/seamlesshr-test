<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SeedCoursesJob;

class SeedCoursesController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        SeedCoursesJob::dispatch();
        return response()->json(['message' => 'Queued 50 courses for seeding'], 201);
    }
}

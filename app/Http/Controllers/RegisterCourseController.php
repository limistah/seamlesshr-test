<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\RegisteredCourses;
use App\Http\Requests\StoreRegisterCourse;
use Illuminate\Support\Facades\Auth;

class RegisterCourseController extends Controller
{
    public function store(StoreRegisterCourse $request)
    {
        $course_id = $request->input("course_id");

        if ($registered_count = RegisteredCourses::where([["course_id", "=", $course_id], ["user_id", "=", Auth::user()->id]])->count()) {
            return response()->json(["message" => "You have registered this course before"], 409);
        }

        $registered_course = new RegisteredCourses();
        $registered_course->course_id = $course_id;
        $registered_course->user_id = Auth::user()->id;
        $registered_course->save();

        return response()->json(["message" => "Course registered", "data" => $registered_course], 201);
    }
}

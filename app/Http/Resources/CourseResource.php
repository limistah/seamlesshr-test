<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $registered = collect($this->registered)->first(
            function ($value, $key) {
                return Auth::user()->id == $value->user->id;
            }
        );

        return [
            'id' => $this->id,
            'text' => $this->text,
            'name' => $this->name,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'user' => $this->user,
            'active' => $this->active,
            'rating' => $this->rating,
            "registered_at" => $registered ? $registered->created_at : null
        ];
    }
}
